package com.example.webshiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebShiroApplication.class, args);
    }

}

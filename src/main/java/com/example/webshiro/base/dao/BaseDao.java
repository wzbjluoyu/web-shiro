package com.example.webshiro.base.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;


import java.io.Serializable;

/**
 * <p>基础持久层</p>
 * @author LuoYu
 * date 2021/2/4
 */
@NoRepositoryBean
public interface BaseDao<E,ID extends Serializable> extends JpaRepository<E, ID>, JpaSpecificationExecutor<E> {




}

package com.example.webshiro.base.dao;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDateTime createTime;

    @Column
    private LocalDateTime updateTime;

    @Column(columnDefinition = "bit default 1")
    private Boolean status;

    @Column(columnDefinition = "bit default 0")
    private Boolean locked;

    @Column(columnDefinition = "bit default 0")
    private Boolean isDeleted;

    @Column(columnDefinition = "bit default 1")
    private Boolean hasDefault;

    @Column
    private String notes;



}

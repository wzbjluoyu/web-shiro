package com.example.webshiro.common.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author LuoYu
 * date 2021/4/12
 */
@Configuration
@ConfigurationProperties(prefix = "auth.jwt")
@Data
public class JwtProperties {


    /**
     * token过期时间  单位s
     */
    private long tokenExpireTime;

    /**
     * 登录用户的长时间不操作的过期时间
     */
    private long currentUserExpireTime;

    /**
     * 签名密钥
     */
    private String signPrivateKey;


}

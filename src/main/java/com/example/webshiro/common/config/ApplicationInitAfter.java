package com.example.webshiro.common.config;

import com.example.webshiro.common.utils.JwtUtil;
import com.example.webshiro.common.utils.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>启动完成后初始化操作</p>
 * @author LuoYu
 * date 2021/4/12
 */
@Component
public class ApplicationInitAfter implements ApplicationRunner {

    private Logger logger = LoggerFactory.getLogger(ApplicationInitAfter.class);

    private static List<Thread> tasks = new ArrayList<>();

    @Override
    public void run(ApplicationArguments args) {

        tasks.add(new Thread(() -> {
            logger.info("初始化Jwt配置信息 ----> Start");
            JwtUtil.setProperties();
            logger.info("Jwt配置信息初始化完毕 ----> End");
        }));
        tasks.add(new Thread(RedisUtil::initPool));
        tasks.forEach(Thread::start);

    }
}

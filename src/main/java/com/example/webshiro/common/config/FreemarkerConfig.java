package com.example.webshiro.common.config;


import com.jagregory.shiro.freemarker.ShiroTags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author LuoYu
 * date 2021/2/5
 */
@Configuration
public class FreemarkerConfig {

    @Autowired
    protected freemarker.template.Configuration configuration;

    @PostConstruct
   public void setShiroTag(){
       configuration.setSharedVariable("shiro",new ShiroTags());

   }



}

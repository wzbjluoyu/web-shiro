package com.example.webshiro.common.config;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * @author LuoYu
 * 2021/4/11
 **/
@Configuration
@Slf4j
@Data
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfig {


    /**
     * 主机
     */
    private String host;
    /**
     * 端口号
     */
    private int port;

    /**
     * 密码
     */
    private String password;

    /**
     * 最大空闲数量
     */
    private int maxIdle=500;

    /**
     * 超时时间
     */
    private int timeout;




    @Bean
    public JedisPool jedisPool() {
        log.info("配置Redis连接池 ----> 连接主机:{} -- 连接端口:{}", host,port);
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxIdle(maxIdle);
        // 连接耗尽时是否阻塞  false抛出异常 true-阻塞
        poolConfig.setBlockWhenExhausted(false);
        final JedisPool jedisPool = new JedisPool(poolConfig,host,port,timeout,password);
        log.info("Redis连接池配置成功....");
        return jedisPool;
    }






}

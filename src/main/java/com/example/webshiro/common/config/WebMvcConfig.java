package com.example.webshiro.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author LuoYu
 * date 2021/2/3
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/index").setViewName("index");
        registry.addViewController("/system/user").setViewName("system/user");
        registry.addViewController("/system/role").setViewName("system/role");
        registry.addViewController("/system/permission").setViewName("system/permission");
        registry.addViewController("/error/401").setViewName("error/401");
        registry.addViewController("/error/500").setViewName("error/500");
        registry.addViewController("/error/404").setViewName("error/404");
    }
}

package com.example.webshiro.common.constant;

/**
 * @author LuoYu
 * date 2021/2/3
 */
public interface CommonConstant {


    /**
     * 成功
     */
    int STATUS_COMPLETE = 0;
    /**
     * 错误
     */
    int STATUS_ERROR = 500;

    /**
     * 当前登录用户key
     */
    String currentRedisPreKey = "current::user::";




}

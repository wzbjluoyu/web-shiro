package com.example.webshiro.common.input;

import lombok.Data;

/**
 * @author LuoYu
 * date 2021/2/5
 */
@Data
public class PageInput {

    private Integer pageIndex;


    private Integer pageSize;

}

package com.example.webshiro.common.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author LuoYu
 * date 2021/2/5
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class PageResponseData<T>{


    private Long count;

    private Integer code;

    private String msg;

    private List<T> data;



}

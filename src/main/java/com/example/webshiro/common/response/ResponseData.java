package com.example.webshiro.common.response;

import com.example.webshiro.common.constant.CommonConstant;
import lombok.Data;

import java.util.Set;

/**
 * @author LuoYu
 * date 2021/2/3
 */
@Data
public class ResponseData<T> {

    private static final String success = "success";


    private Integer code;

    private String msg;

    private T data;


    public static <T> ResponseData<T> success(T data){
        ResponseData<T> responseData = new ResponseData<>();
        responseData.setData(data);
        responseData.setMsg(success);
        responseData.setCode(CommonConstant.STATUS_COMPLETE);
        return responseData;
    }

    public static <T> ResponseData<T> error(String msg){
        ResponseData<T> responseData = new ResponseData<>();
        responseData.setMsg(msg);
        responseData.setCode(CommonConstant.STATUS_ERROR);
        return responseData;
    }




}

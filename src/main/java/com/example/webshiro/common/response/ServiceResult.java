package com.example.webshiro.common.response;

import com.example.webshiro.common.constant.CommonConstant;
import lombok.Data;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Data
public class ServiceResult<T> {

    private static final String success = "success";

    private Integer code;

    private String msg;

    private T data;

    public static <T> ServiceResult<T> success(T data){
        return new ServiceResult<>(CommonConstant.STATUS_COMPLETE,success,data);
    }

    public ServiceResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ServiceResult<T> error(String msg){
        return new ServiceResult<T>(CommonConstant.STATUS_ERROR, msg, null);
    }


    public boolean isSuccess(){
        return this.code == CommonConstant.STATUS_COMPLETE;
    }



}

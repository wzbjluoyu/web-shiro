package com.example.webshiro.common.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * <p>base64加密解密工具</p>
 * @author LuoYu
 * date 2021/4/12
 */
public class Base64EncyUtil {


    /**
     * <p>base64加密</p>
     * @param str  加密字符串
     * @return      加密后的字符串
     */
    public static String encode(String str){
        final byte[] bytes = Base64.getEncoder().encode(str.getBytes(StandardCharsets.UTF_8));
        return new String(bytes);
    }


    /**
     * <p>base64解密</p>
     * @param str   加密字符串
     * @return       解密后的字符串
     */
    public static String decode(String str){
        final byte[] bytes = Base64.getDecoder().decode(str.getBytes(StandardCharsets.UTF_8));
        return new String(bytes);
    }


}

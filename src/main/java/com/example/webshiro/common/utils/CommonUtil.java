package com.example.webshiro.common.utils;

import com.example.webshiro.common.constant.CommonConstant;
import com.example.webshiro.common.response.ResponseData;

import java.util.Collection;


/**
 * @author LuoYu
 * 2021/4/11
 **/
public class CommonUtil {


    /**
     * <p>集合是否为空</p>
     * @param collection  集合
     * @param <T>         泛型
     * @return            true-为空    false-不为空
     */
    public static <T>  boolean isEmptyCollection(Collection<T> collection){
        return collection == null || collection.size() == 0;
    }


    /**
     * <p>字符串是否为空</p>
     * @param str  字符串
     * @return      true-为空    false-不为空
     */
    public static boolean isEmptyStr(String str){
        if(str == null){
            return true;
        }
        return str.isEmpty();
    }


    /**
     * <p>返回错误的请求</p>
     * @param <T>  泛型
     * @return
     */
    public static <T> ResponseData<T> errorResponse(String msg){
        ResponseData<T> data = new ResponseData<>();
        data.setCode(CommonConstant.STATUS_ERROR);
        data.setMsg(msg);
        return data;
    }





}

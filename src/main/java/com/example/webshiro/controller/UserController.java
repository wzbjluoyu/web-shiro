package com.example.webshiro.controller;


import com.example.webshiro.common.constant.CommonConstant;
import com.example.webshiro.common.response.PageResponseData;
import com.example.webshiro.common.response.ResponseData;
import com.example.webshiro.common.utils.CommonUtil;
import com.example.webshiro.common.utils.JwtUtil;
import com.example.webshiro.common.utils.RedisUtil;
import com.example.webshiro.server.user.model.User;
import com.example.webshiro.server.user.service.IUserService;
import com.example.webshiro.shiro.CurrentUserInfo;
import lombok.Data;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

/**
 * @author LuoYu
 * date 2021/2/2
 */
@Controller
public class UserController {





    @Resource
    private IUserService userService;


    @PostMapping(value = "/user/login")
    @ResponseBody
    public ResponseData<String> login(@RequestBody @Validated LoginInput input){
        User userInfo = userService.getUserInfo(input.getUsername());
        if(userInfo == null){
            return CommonUtil.errorResponse("用户不存在或者密码错误");
        }
        if(!input.getPassword().equals(userInfo.getPassword())) {
            return CommonUtil.errorResponse("用户不存在或者密码错误");
        }
        CurrentUserInfo currentUserInfo = new CurrentUserInfo();
        currentUserInfo.setId(userInfo.getId());
        currentUserInfo.setUuid(UUID.randomUUID().toString().replace("-", ""));
        //登录成功 获取签名
        String sign = JwtUtil.sign(currentUserInfo);
        //设置redis
        String key = CommonConstant.currentRedisPreKey.concat(String.valueOf(userInfo.getId()));
        RedisUtil.set(key, currentUserInfo.getUuid(), 1800);
        return ResponseData.success(sign);


    }



    @GetMapping(value = "/user/page")
    @RequiresAuthentication
    @RequiresRoles(value = "admin")
    @ResponseBody
    public PageResponseData<User> page(@RequestParam(value = "page",defaultValue = "1") Integer page,@RequestParam(value = "limit",defaultValue = "10") Integer limit){
        PageRequest request =  PageRequest.of(page -1,limit);
        Page<User> userPage = userService.findAll(request);
        PageResponseData<User> responseData = new PageResponseData<>();
        responseData.setCount(userPage.getTotalElements());
        responseData.setData(userPage.getContent());
        responseData.setCode(CommonConstant.STATUS_COMPLETE);
        return responseData;
    }










    @GetMapping(value = "/test")
    @ResponseBody
    @RequiresAuthentication
    public ResponseData<String> getTest(){
        return ResponseData.success("success");
    }

    @GetMapping(value = "/test1")
    @ResponseBody
    @RequiresAuthentication
    @RequiresRoles(value = {"admin"})
    public ResponseData<String> getTest1(){
        return ResponseData.success("success");
    }

    @GetMapping(value = "/test2")
    @ResponseBody
    @RequiresAuthentication
    @RequiresPermissions(value = {"add"})
    public ResponseData<String> getTest2(@RequestParam(value = "id",required = false) Integer id){
        return ResponseData.success("success");
    }

    @GetMapping(value = "/logout")
    public String logout(){
        SecurityUtils.getSubject().logout();
        return "redirect:login";
    }



}

@Data
class LoginInput{

    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

}

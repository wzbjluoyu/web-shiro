package com.example.webshiro.server.permission.model;

import com.example.webshiro.base.dao.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@EqualsAndHashCode(callSuper = false)
@Entity(name = "permission")
@Data
public class Permission extends BaseEntity {


    @Column(length = 50,nullable = false,unique = true)
    private String name;

    @Column(length = 50,nullable = false,unique = true)
    private String displayName;

    @Column(length = 50,nullable = false)
    private String permissionType;

    @Column(columnDefinition = "bit default 1")
    private Boolean hasDefault;

}

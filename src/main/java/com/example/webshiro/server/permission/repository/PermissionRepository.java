package com.example.webshiro.server.permission.repository;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.permission.model.Permission;

/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface PermissionRepository  extends BaseDao<Permission,Long> {
}

package com.example.webshiro.server.permission.service;

import com.example.webshiro.base.dao.BaseService;
import com.example.webshiro.server.permission.model.Permission;

/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface IPermissionService extends BaseService<Permission,Long> {
}

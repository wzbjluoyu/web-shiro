package com.example.webshiro.server.permission.service.impl;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.permission.model.Permission;
import com.example.webshiro.server.permission.repository.PermissionRepository;
import com.example.webshiro.server.permission.service.IPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Service
@Slf4j
public class PermissionServiceImpl implements IPermissionService {


    @Resource
    private PermissionRepository permissionRepository;


    @Override
    public BaseDao<Permission, Long> getDao() {
        return this.permissionRepository;
    }
}

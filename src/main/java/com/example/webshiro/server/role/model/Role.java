package com.example.webshiro.server.role.model;

import com.example.webshiro.base.dao.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author LuoYu
 * date 2021/2/3
 */
@EqualsAndHashCode(callSuper = false)
@Entity(name = "role")
@Data
public class Role extends BaseEntity {

    @Column(length = 50,nullable = false,unique = true)
    private String name;

    @Column(length = 50,nullable = false,unique = true)
    private String displayName;

    @Column(columnDefinition = "bit default 1")
    private Boolean hasDefault;

    @Column
    private LocalDateTime createTime;

    @Column
    private LocalDateTime updateTime;

    @Column(columnDefinition = "bit default 1")
    private Boolean status;

    @Column(columnDefinition = "bit default 0")
    private Boolean locked;

    @Column(columnDefinition = "bit default 0")
    private Boolean isDeleted;

}

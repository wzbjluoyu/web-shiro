package com.example.webshiro.server.role.model;

import com.example.webshiro.base.dao.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@EqualsAndHashCode(callSuper = false)
@Entity(name = "role_link_permission")
@Data
public class RoleLinkPermission extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long roleId;

    @Column(nullable = false)
    private Long permissionId;


}

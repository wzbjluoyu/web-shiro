package com.example.webshiro.server.role.repository;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.role.model.RoleLinkPermission;

/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface RoleLinkPermissionRepository extends BaseDao<RoleLinkPermission,Long> {
}

package com.example.webshiro.server.role.repository;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.role.model.Role;


/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface RoleRepository extends BaseDao<Role,Long> {


}

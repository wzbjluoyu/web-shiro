package com.example.webshiro.server.role.service;

import com.example.webshiro.base.dao.BaseService;
import com.example.webshiro.server.role.model.RoleLinkPermission;

/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface IRoleLinkPermissionService extends BaseService<RoleLinkPermission,Long> {
}

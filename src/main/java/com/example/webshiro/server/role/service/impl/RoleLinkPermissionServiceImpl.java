package com.example.webshiro.server.role.service.impl;
import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.role.model.RoleLinkPermission;
import com.example.webshiro.server.role.repository.RoleLinkPermissionRepository;
import com.example.webshiro.server.role.service.IRoleLinkPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Service
@Slf4j
public class RoleLinkPermissionServiceImpl implements IRoleLinkPermissionService {

    @Resource
    private RoleLinkPermissionRepository roleLinkPermissionRepository;

    @Override
    public BaseDao<RoleLinkPermission, Long> getDao() {
        return this.roleLinkPermissionRepository;
    }
}

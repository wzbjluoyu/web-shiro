package com.example.webshiro.server.role.service.impl;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.role.model.Role;
import com.example.webshiro.server.role.repository.RoleRepository;
import com.example.webshiro.server.role.service.IRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Service
@Slf4j
public class RoleServiceImpl implements IRoleService {

    @Resource
    private RoleRepository roleRepository;


    @Override
    public BaseDao<Role, Long> getDao() {
        return this.roleRepository;
    }
}

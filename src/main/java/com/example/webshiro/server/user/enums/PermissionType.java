package com.example.webshiro.server.user.enums;

import cn.hutool.core.util.StrUtil;

/**
 * <p>权限类型枚举</p>
 * @author LuoYu
 * date 2021/2/4
 **/
public enum PermissionType {
    PERMISSION_TYPE_MENU("menu","菜单"),PERMISSION_TYPE_BUTTON("button","按钮");


    private String type;

    private String text;


    PermissionType(String type, String text) {
        this.type = type;
        this.text = text;
    }


    public static PermissionType getEnum(String type){
        if(StrUtil.hasBlank(type)){
            return null;
        }
        PermissionType[] values = PermissionType.values();
        for (PermissionType value : values) {
            if(!StrUtil.isNotBlank(value.getType(type))){
                return value;
            }
        }
        return null;
    }


    public String getType(String type){
        return this.type;
    }





}

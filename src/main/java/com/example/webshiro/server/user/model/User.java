package com.example.webshiro.server.user.model;

import com.example.webshiro.base.dao.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author LuoYu
 * date 2021/2/2
 */
@EqualsAndHashCode(callSuper = false)
@Entity(name = "user")
@Data
public class User  extends BaseEntity {


    @Column(length = 50,unique = true,nullable = false)
    private String username;

    @Column(length = 50,nullable = false)
    private String password;

    @Column(length = 50)
    private String nickName;

    @Column(length = 50)
    private String phone;

    @Column(length = 50)
    private String idCard;

    @Column(length = 10)
    private String sex;

    @Column(length = 50)
    private String trueName;



}

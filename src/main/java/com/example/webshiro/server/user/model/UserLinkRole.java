package com.example.webshiro.server.user.model;

import com.example.webshiro.base.dao.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@EqualsAndHashCode(callSuper = false)
@Entity(name = "user_link_role")
@Data
public class UserLinkRole extends BaseEntity {


    @Column
    private Long userId;

    @Column
    private Long roleId;


}

package com.example.webshiro.server.user.repository;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.user.model.UserLinkRole;

/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface UserLinkRoleRepository extends BaseDao<UserLinkRole,Long> {
}

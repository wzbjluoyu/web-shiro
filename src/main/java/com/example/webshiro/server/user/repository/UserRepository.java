package com.example.webshiro.server.user.repository;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.user.model.User;


/**
 * @author LuoYu
 * date 2021/2/2
 */
public interface UserRepository extends BaseDao<User,Long> {

}

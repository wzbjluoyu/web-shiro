package com.example.webshiro.server.user.service;

import com.example.webshiro.base.dao.BaseService;
import com.example.webshiro.server.user.model.UserLinkRole;

/**
 * @author LuoYu
 * date 2021/2/4
 **/
public interface IUserLinkRoleService extends BaseService<UserLinkRole,Long> {
}

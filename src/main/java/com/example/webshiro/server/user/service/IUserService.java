package com.example.webshiro.server.user.service;

import com.example.webshiro.base.dao.BaseService;
import com.example.webshiro.server.user.model.User;

/**
 * @author LuoYu
 * date 2021/2/4
 */
public interface IUserService extends BaseService<User,Long> {


    User getUserInfo(String username);


}

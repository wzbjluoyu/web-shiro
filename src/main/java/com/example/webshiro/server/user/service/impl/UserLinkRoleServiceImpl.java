package com.example.webshiro.server.user.service.impl;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.user.model.UserLinkRole;
import com.example.webshiro.server.user.repository.UserLinkRoleRepository;
import com.example.webshiro.server.user.service.IUserLinkRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Service
@Slf4j
public class UserLinkRoleServiceImpl implements IUserLinkRoleService {

    @Resource
    private UserLinkRoleRepository userLinkRoleRepository;

    @Override
    public BaseDao<UserLinkRole, Long> getDao() {
        return this.userLinkRoleRepository;
    }




}

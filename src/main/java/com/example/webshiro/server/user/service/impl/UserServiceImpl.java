package com.example.webshiro.server.user.service.impl;

import com.example.webshiro.base.dao.BaseDao;
import com.example.webshiro.server.user.model.User;
import com.example.webshiro.server.user.repository.UserRepository;
import com.example.webshiro.server.user.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LuoYu
 * date 2021/2/4
 */
@Service
@Slf4j
public class UserServiceImpl implements IUserService {


    @Resource
    private UserRepository repository;


    @Override
    public BaseDao<User, Long> getDao() {
        return this.repository;
    }

    @Override
    public User getUserInfo(String username) {
        Specification specification = (Specification) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            list.add(criteriaBuilder.equal(root.get("username").as(String.class), username));
            Predicate[] pre  = new Predicate[list.size()];
            pre = list.toArray(pre);
            return criteriaQuery.where(pre).getRestriction();
        };
        return getByCondition(specification);
    }
}

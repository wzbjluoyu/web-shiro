package com.example.webshiro.shiro;

import com.example.webshiro.server.role.model.Role;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author LuoYu
 * date 2021/2/3
 */
@Data
public class CurrentUserInfo implements Serializable {
    private Long id;
    private List<Role> roles;
    private String trueName;
    private List<String> permissions;
    private String uuid;
}

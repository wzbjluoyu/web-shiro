package com.example.webshiro.shiro.service;

import com.example.webshiro.common.response.ServiceResult;
import com.example.webshiro.server.permission.model.Permission;
import com.example.webshiro.shiro.CurrentUserInfo;

import java.util.List;

/**<p>授权管理service</p>
 * @author LuoYu
 * date 2021/2/4
 **/
public interface IAuthService {


    ServiceResult<CurrentUserInfo> getUserInfo(String username);


    ServiceResult<List<Permission>> getPermissions(List<Long> roleIds);










}

layui.use('table', function(){
    var table = layui.table;

    table.render({
        elem: '#test'
        ,url:'/user/page'
        ,cols: [[
            {title:'序号', field:'zizeng', templet:'#zizeng',type:'number',width:80}
            ,{field:'id', hide:true,width:80}
            ,{field:'username', width:150, title: '用户名'}
            ,{field:'trueName', width:150, title: '姓名'}
            ,{field:'idCard', width:200, title: '身份证'}
            ,{field:'phone', title: '联系方式',width:150}
            ,{field:'sex', width:100, title: '性别'}
            ,{field:'role', width:150, title: '角色'}
            ,{field:'createTime', width:200, title: '创建时间', sort: true}
            ,{fixed: 'right', title:'操作', toolbar: '#barDemo'}
        ]]
        ,page: true
    });


    //监听行工具事件
    table.on('tool(test)', function(obj){
        var data = obj.data;
        console.log(obj)
        if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                obj.del();
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            layer.prompt({
                formType: 2
                ,value: data.email
            }, function(value, index){
                obj.update({
                    email: value
                });
                layer.close(index);
            });
        }
    });
















});
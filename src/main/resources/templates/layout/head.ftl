<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>主页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/layui/css/layui.css"  media="all">
    <link rel="stylesheet" href="/css/layout.css" media="all">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">Web-shiro</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="/img/head-icon.jpg" class="layui-nav-img">
                    Boy
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">修改密码</a></dd>
                </dl>
            </li>
            <li  class="layui-nav-item"><a href="/logout">注销</a></li>
        </ul>
    </div>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item"><a href="">主页</a></li>
                <@shiro.hasRole name="admin">
                <li class="layui-nav-item">
                    <a href="javascript:;">系统管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/system/user">用户管理</a></dd>
                        <dd><a href="/system/role">角色管理</a></dd>
                        <dd><a href="/system/permission">权限管理</a></dd>
                    </dl>
                </li>
                </@shiro.hasRole>
            </ul>
        </div>
    </div>
    <#--主体部分  body-start-->
    <div class="layui-body">
     <div class="body-header">
         <div class="body-header-container">
             <span class="layui-breadcrumb">
              <a href="/index">首页</a>
              <a href="/demo/">系统管理</a>
              <a><cite>${navTitle}</cite></a>
            </span>
         </div>
     </div>
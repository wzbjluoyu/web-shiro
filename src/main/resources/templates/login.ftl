<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="/css/login.css" />
</head>

<body >
<div class="content">
    <div class="box">
        <div class="left">
            <img class="login-img" alt="Login" src="img/login.svg" />
        </div>
        <div class="right">
            <h1 class="title">User Login</h1>
            <div class="username">
                <label for="username">User Name</label>
                <input
                        type="text"
                        id="username"
                        name="username"
                        autocomplete="off"
                        placeholder="Username"
                />
            </div>
            <div class="password">
                <label for="password">Password</label>
                <input
                        type="password"
                        id="password"
                        name="password"
                        autocomplete="off"
                        placeholder="Password"
                />
            </div>
            <button id="loginBtn" class="submit">LOGIN IN</button>
        </div>
    </div>
</div>
</body>
<script src="/js/jquery-2.0.2.min.js"></script>
<script>
    $("#loginBtn").click(function () {
        let data = {username:$("#username").val(),password:$("#password").val()};
        $.ajax({
            type:'post',
            url:'/user/login',
            data:JSON.stringify(data),
            dataType:'json',
            contentType:'application/json',
            success:function (result) {
                if (result !== undefined && result.code === 0){
                    window.location.href = "/index";
                }
            }
        })
    })







</script>
</html>
<!doctype html>
<html lang="en">
<#assign title="主页">
<#assign navTitle = "用户管理">
<#include "../layout/head.ftl">
<div class="body-input">
    <div class="body-input-container">
        <div class="layui-inline">
            <label class="layui-form-label">用户姓名</label>
            <div class="layui-input-inline">
                <input type="tel" name="phone"  autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-inline">
            <label class="layui-form-label">创建日期</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="test1" placeholder="yyyy-MM-dd">
            </div>
        </div>
        <button style="margin-left: 20px" type="button" class="layui-btn">查询</button>
        <div style="display: flex;flex: 1;justify-content: flex-end"><button type="button" class="layui-btn layui-btn-normal">新增用户</button></div>

    </div>
</div>
<table class="layui-hide" id="test"></table>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/html" id="zizeng">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<#include "../layout/footer.ftl"/>